﻿using System;
using System.IO;
using FileMonitoring.Interfaces;
using System.Threading;

namespace FileMonitoring
{
	public class FileMonitor : IFileMonitor
	{
		private FileSystemWatcher fileMonitor;

		private string actualPath;

		private string backupRef;

		private int changeCheker;

		//private string revisionNum;

		public FileMonitor(IConfiguration configuration)
		{
			actualPath = configuration.Path;
			backupRef = configuration.BackupPath;
			Directory.CreateDirectory(backupRef);
			fileMonitor = new FileSystemWatcher(actualPath);
		}

		public void Start()
		{
			fileMonitor.NotifyFilter = NotifyFilters.Attributes | NotifyFilters.LastWrite;

			fileMonitor.Filter = "*.txt";
			fileMonitor.Changed += OnChanged;
				
			fileMonitor.IncludeSubdirectories = true;
			fileMonitor.EnableRaisingEvents = true;

            foreach (var file in Directory.EnumerateFiles(actualPath))
            {
				if (Path.GetFileName(file).Contains(".txt"))
				{
					Directory.CreateDirectory(Path.Combine(backupRef, Path.GetFileName(file)));
					File.Copy(file, Path.Combine(Path.Combine(backupRef, Path.GetFileName(file)), Path.GetFileName(file)), true);
					Thread.Sleep(2000);
				}
            }

        }



        public void Stop()
		{

			fileMonitor.Changed -= OnChanged;
			
			Directory.Delete(backupRef, true);

		}

		//public void Reset(DateTime dateTime)
		//{
		//	if (Directory.Exists(actualPath))
		//	{
		//		foreach (var file in Directory.EnumerateFiles(backupRef))
		//		{
		//			if (File.GetCreationTime(file) <= dateTime)
		//			{
		//				fileMonitor.Changed -= OnChanged;

		//				if (Path.GetFileNameWithoutExtension(file).Contains("rev"))
		//				{
		//					char[] shortName = Path.GetFileName(file).													//да простит меня Чернов Дима, ибо осознаю я, что решение принял не 
		//											ToCharArray(revisionNum.Length,
		//											Path.GetFileName(file).Length - revisionNum.Length);				// самое лучшее. Но конфликт имен файловой системы - не мой конек.
		//					string newName = "";																		//Да можно было создавать внутри папки _backup отдельные директории,
		//					foreach(var i in shortName)																	//хранить бэкапы по времени создания, создать мощную систему ветвления
		//					{ newName += i; }																			//но в связи с сильным отставанием от обучающей программы, не успеваю я.
		//																												//Дима! Я обязуюсь самостоятельно изучить суть вопроса, и переделать метод, когда будет время)
		//				File.Copy(file, Path.Combine(actualPath, newName), true);}
		//				else { File.Copy(file, Path.Combine(actualPath, Path.GetFileName(file)), true); }


		//				fileMonitor.Changed += OnChanged;
		//			}
		//		}
		//	}
		//}

		public void Reset (DateTime dateTime)
        {
			foreach (var dir in Directory.EnumerateDirectories(backupRef))
            {
				if (Directory.GetCreationTime(dir)<=dateTime)
                {
					foreach (var file in Directory.EnumerateFiles(dir))
					{ 
						fileMonitor.Changed -= OnChanged;
						File.Copy(file, Path.Combine(actualPath, Path.GetFileName(file)), true);
						Thread.Sleep(2000);
						fileMonitor.Changed += OnChanged;
					}

                }
            }
        }
		public void Dispose()
		{
			fileMonitor.Changed -= OnChanged;
			
			Directory.Delete(backupRef, true);
		}

		//     public void OnChanged(object sender, FileSystemEventArgs e)
		//     {
		//if (e.ChangeType == WatcherChangeTypes.Changed)
		//{

		//	changeCheker += 1;
		//	revisionNum = "rev" + changeCheker.ToString();
		//	File.Copy(e.FullPath, Path.Combine(backupRef, revisionNum
		//															+ e.Name), true);
		//}

		//     }

		public void OnChanged(object sender, FileSystemEventArgs e)
        {
			if (e.ChangeType == WatcherChangeTypes.Changed)
			{
				changeCheker += 1;
				Directory.CreateDirectory(Path.Combine(backupRef, e.Name+changeCheker.ToString()));
				File.Copy(e.FullPath, Path.Combine(Path.Combine(backupRef, e.Name + changeCheker.ToString()), 
																		e.Name), true);
				Thread.Sleep(2000);
			}
		}


	}
}
